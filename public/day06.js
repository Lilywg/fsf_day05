window.addEventListener('load', init, false);
function init() {
    button = document.getElementById("button");
    input = document.getElementById("input");
    button.style.backgroundColor = "lightblue";
    input.style.backgroundColor = "aqua";
    button.addEventListener('click', play, false);
}

function play() {
    var card =
        ["0CA.png", "0C2.png", "0C3.png", "0C4.png", "0C5.png", "0C6.png", "0C7.png", "0C8.png", "0C9.png", "0C10.png", "0CJ.png", "0CQ.png", "0CK.png",
         "0HA.png", "0H2.png", "0H3.png", "0H4.png", "0H5.png", "0H6.png", "0H7.png", "0H8.png", "0H9.png", "0H10.png", "0HJ.png", "0HQ.png", "0HK.png",
         "0SA.png", "0S2.png", "0S3.png", "0S4.png", "0S5.png", "0S6.png", "0S7.png", "0S8.png", "0S9.png", "0S10.png", "0SJ.png", "0SQ.png", "0SK.png",
         "0DA.png", "0D2.png", "0D3.png", "0D4.png", "0D5.png", "0D6.png", "0D7.png", "0D8.png", "0D9.png", "0D10.png", "0DJ.png", "0DQ.png", "0DK.png"];

    document.write("<h2>You have chosen " + input.value + "x shuffle!</h2>");
    document.write("<h2>Before Shuffled:</h2>");
    for (var x = 0; x < card.length; x++) {
        document.write("<img height = 80px width = 60px src = '" + card[x] + "'>");
        if (x == 12 || x == 25 || x == 38) {
            document.write("<br>");
        }
    }

    Array.prototype.shuffle = function () {
        var myInput = this;

        for (var i = myInput.length - 1; i >= 0; i--) {

            var randomIndex = Math.floor(Math.random() * (i + 1));
            var itemAtIndex = myInput[randomIndex];

            myInput[randomIndex] = myInput[i];
            myInput[i] = itemAtIndex;
        }
        return myInput;
    }

    for (var y = 0; y < input.value; y++) {
        card.shuffle();
        document.write("<h2> After " + (y + 1) + "x Shuffled:</h2>");

        for (x = 0; x < card.length; x++) {
            if (y==input.value-1 && x==0)
                document.write("<img height = 80px width = 60px src = '" + card[x] + "'>");
            else {
            document.write("<img height = 80px width = 60px src = '" + 'BackCard.png' + "'>");
            if (x == 12 || x == 25 || x == 38) {
                document.write("<br>");
            }
            }
         }
    }

    document.write("<br><br>");
    document.write("<h2> This is your lucky card: <img height = 80px width = 60px border = red src = '" + card[0] + "'></h2>");
}