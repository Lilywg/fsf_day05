//load express
var express = require ("express");

//create an express app
var app = express();

//specify doc root - static files
app.use(express.static(__dirname + "/public"));

//listen to port 3000
app.listen(3000, function () {
   console.info("Application started on port 3000");
   console.info("Cntl + C to terminate");
});